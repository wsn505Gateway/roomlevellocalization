package com.example.roomlevellocalization;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.example.common.CpuUsage;
import com.example.common.FileService;
import com.example.service.WiFiSwitch;
import com.example.util.HttpRequest;

import android.support.v7.app.ActionBarActivity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private Button btnStart,btnClose,btnShow;
	private TextView textView1;
	
	public static int  reconectTime = 10*1000;
	public static int uiFreshtime =1000;
	
	WiFiSwitch.MyBinder binder;
	
	FileService fileService ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
	
		fileService = new FileService(MainActivity.this);
		 
		btnStart = (Button)findViewById(R.id.btnStart);
		btnClose = (Button)findViewById(R.id.btnClose);	
		textView1 = (TextView)findViewById(R.id.textView1);			
	
		final Intent intent = new Intent(this,WiFiSwitch.class);
		
		btnStart.setOnClickListener(
			new OnClickListener(){
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					bindService(intent, con, Service.BIND_AUTO_CREATE);	
				}});
		
		btnClose.setOnClickListener(
				
				new OnClickListener(){
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						unbindService(con);
					}});
		
				final Handler myHandler = new Handler(){
					@Override
					public void handleMessage(Message msg){
						if(msg.what==0x1233){			
							if(binder!=null)	{
								System.out.println(binder.GetWiFiInfo());
								//textView1.setText(binder.GetWiFiInfo());
								textView1.setText(binder.GetWiFiInfo()+"\n CPU使用率："+CpuUsage.getProcessCpuRate()+"\n ");
							   try {
								SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
								String str_ToSend = "Date="+ df.format(new Date())+"&"+binder.GetWiFiInfo()+"&CPU="+CpuUsage.getProcessCpuRate();
								//HttpRequest.sendGet("receive", str_ToSend);
								fileService.saveAppend("CpuUage.txt", "hello");
								//fileService.saveAppend("CpuUage.txt", df.format(new Date())+ "\n"+binder.GetWiFiInfo()+ "CPU使用率："+CpuUsage.getProcessCpuRate()+"\n \n");
								System.out.println("CpuUage.txt数据发送成功！");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							}
						}
					}
				};
				
				//定义一个计时器，让该计时器周期性执行任务
				Timer timer = new Timer();
				timer.schedule( new TimerTask(){
					@Override
					public void run() {
						// TODO Auto-generated method stub
						 myHandler.sendEmptyMessage(0x1233);
					}}, 0, uiFreshtime);//每个1S向
	}	
	
	private ServiceConnection con = new ServiceConnection(){		
		
		//当activity与service连接成功时回调该方法
		@Override
			public void onServiceConnected(ComponentName arg0, IBinder service) {
				// TODO Auto-generated method stub
				System.out.println("Service is Connected!");
				binder = (com.example.service.WiFiSwitch.MyBinder)service;
			}

			@Override
			public void onServiceDisconnected(ComponentName arg0) {
				// TODO Auto-generated method stub
				System.out.println();
			}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
