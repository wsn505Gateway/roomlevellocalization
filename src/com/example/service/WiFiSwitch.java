package com.example.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import com.example.roomlevellocalization.MainActivity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;


public class WiFiSwitch  extends Service {

	 public String mssid="SmartTravel";
	 private WifiManager wifiManager;  
	 private Timer timer;
	 
	 private String minDistinceAP;
	 private int maxRss;
	 
	 private MyBinder binder = new MyBinder();
	 public class MyBinder extends Binder{
		 
		 //获取当前连接AP的信息
		public String GetWiFiInfo(){
			
			wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
			//获取当前连接的SSID，BSSID
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();		
			String currentSSID = wifiInfo.getSSID();//获取当前wifi的ssid
		    String currentBssid = wifiInfo.getBSSID();
		    int curlevel = wifiInfo.getRssi();
		    String return_Str = "SSID="+currentSSID+"&BSSID="+currentBssid+"&RSSI="+curlevel;
		    System.out.println(return_Str);
		    //返回格式化的连接信息
		    return return_Str;
		    
		}		
	 }
	 
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		Log.v("LocationService", "Service is binded");
		return binder;
	}

	@Override
	public void onCreate(){
		super.onCreate();

		wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
		timer = new Timer();
		timer.schedule( new TimerTask()
		{
			@Override
			public void run() {				
				// TODO Auto-generated method stub
				if (!wifiManager.isWifiEnabled()){  
			        wifiManager.setWifiEnabled(true); 
			    }  				
				wifiManager.reassociate();
			}}, 0, MainActivity.reconectTime);	//3s检测一次，重连一次
	}
	
	@Override
	public int onStartCommand(Intent intent,int flags,int startId){
		return START_STICKY;
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Log.v("LocationService", "Service is Unbinded");
	}
//	 class StartScan implements Runnable 
//	    {  
//	    	public String mSSid;
//	    	public StartScan(String ssid)
//	    	{
//	    		mSSid = ssid;
//	    	}
//	    	private void caculateWifi()
//	    	{
//	            for(Iterator<ScanResult> result = list.iterator();result.hasNext(); )
//	            {
//	            	ScanResult scanresult = result.next();
//					if(!scanresult.SSID.equals(mSSid))
//	            		continue;
//	            	if(!rssfingers.addrRss.containsKey(scanresult.BSSID))
//	            	{
//	            		rssfingers.addrRss.put(scanresult.BSSID,new ArrayList<Integer>());
//	            	}
//	            	rssfingers.addrRss.get(scanresult.BSSID).add(new Integer(scanresult.level+96));
//	            }
//	    	}
//	    	@Override
//	    	public void run()
//	    	{
//	    		caculateWifi();
//	    	}
//	    }
}
